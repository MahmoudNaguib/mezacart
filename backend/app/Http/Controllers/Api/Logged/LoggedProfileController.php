<?php

namespace App\Http\Controllers\Api\Logged;

use App\Http\Controllers\Controller;
use Hash;
use Validator;

class LoggedProfileController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public $model;

    public function __construct(\App\Models\User $model) {
        $this->model = $model;
        $this->editProfileRules = $model->editProfileRules;
        $this->changePasswordRules = $model->changePasswordRules;
        $this->changeImageRules = $model->changeImageRules;
    }

    public function getIndex() {
        return new \App\Http\Resources\UserResource($this->model->includes()->findOrFail(auth()->user()->id));
    }

    public function postEdit() {
        $row = \App\Models\User::findOrFail(auth()->user()->id);
       // $this->editProfileRules['email'] .= ',' . $row->id . ',id,deleted_at,NULL';
        $validator = Validator::make(request()->all(), $this->editProfileRules);
        if ($validator->fails()) {
            return response()->json(transformValidation($validator->errors()->messages()), 422);
        }
        if ($row->update(request()->except(['password']))) {
            return new \App\Http\Resources\UserResource($this->model->includes()->findOrFail($row->id));
        }
        return response()->json(['message' => trans('app.Failed to edit profile')], 400);
    }

    public function postChangePassword() {
        $row = $this->model->findOrFail(auth()->user()->id);
        $validator = Validator::make(request()->all(), $this->changePasswordRules);
        if ($validator->fails()) {
            return response()->json(transformValidation($validator->errors()->messages()), 422);
        }
        ////////////////////////////// refresh the token
        $hash = md5(time()) . md5($row->email) . md5(RandomString(10));
        request()->request->add([
            'token'=>$hash,
        ]);
        if ($row->update(request()->only(['password','token']))) {
            request()->headers->set('token', $hash);
            return new \App\Http\Resources\UserResource($this->model->includes()->findOrFail($row->id));
        }
        return response()->json(['message' => trans('app.Failed to edit change password')], 400);
    }

    public function postChangeImage() {
        $row = auth()->user();
        $validator = Validator::make(request()->all(), $this->changeImageRules);
        if ($validator->fails()) {
            return response()->json(transformValidation($validator->errors()->messages()), 422);
        }
        if ($row->update(request()->only(['image']))) {
            return new \App\Http\Resources\UserResource($this->model->includes()->findOrFail($row->id));
        }
        return response()->json(['message' => trans('app.Failed to change image')], 400);
    }

    public function getLogout() {
        $this->model->where('id',auth()->user()->id)->update(['token'=>NULL]);
        auth()->logout();
        return response()->json(['message' => trans('app.Log out successfully')], 200);
    }

}
