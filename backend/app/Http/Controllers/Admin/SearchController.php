<?php

namespace App\Http\Controllers\Admin;

class SearchController extends \App\Http\Controllers\Controller {

    public function __construct() {
        $this->module = 'search';
        $this->title = trans('admin.Search results');
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = $this->title;
        if (request('q')) {
            $data['posts'] = \App\Models\Post::search(trim(request('q')))->get();
            $data['gallery'] = \App\Models\Gallery::search(trim(request('q')))->get();
            $data['pages'] = \App\Models\Page::search(trim(request('q')))->get();
            $data['products'] = \App\Models\Product::search(trim(request('q')))->get();
            $data['services'] = \App\Models\Service::search(trim(request('q')))->get();
            $data['why'] = \App\Models\Why::search(trim(request('q')))->get();
            $data['users'] = \App\Models\User::search(trim(request('q')))->get();
            $data['messages'] = \App\Models\Message::search(trim(request('q')))->get();
            $data['contacts'] = \App\Models\Contact::search(trim(request('q')))->get();
        }
        return view('admin.' . $this->module . '.index', $data);
    }

}
