<?php

namespace App\Http\Controllers\Admin;

class DashBoardController extends \App\Http\Controllers\Controller {

    public function __construct() {
        $this->module = 'dashboard';
    }

    public function getIndex() {
        $section = new \App\Models\Section();
        $data['page_title'] = trans('admin.Dashboard');
        $data['total_contacts'] = \App\Models\Contact::count();
        $data['total_messages'] = \App\Models\Message::count();
        $data['total_posts'] = \App\Models\Post::count();
        $data['messages'] = \App\Models\Message::latest()->limit(5)->get();
        $data['contacts'] = \App\Models\Contact::latest()->limit(5)->get();
        $data['posts'] = \App\Models\Post::latest()->limit(5)->get();
        return view('admin.' . $this->module . '.index', $data);
    }

}
