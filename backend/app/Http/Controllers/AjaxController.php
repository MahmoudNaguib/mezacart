<?php

namespace App\Http\Controllers;

use Dompdf\Exception;

class AjaxController extends Controller {

    public $model;
    public $module;

    public function postSendContactUsMessage(\App\Models\Message $model) {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'content' => 'required',
        ];
        if (env('ENABLE_CAPTCHA') == 1 && app()->environment() == 'production') {
            $rules['g-recaptcha-response'] = 'required';
        }
        $validator = \Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            return response()->json(transformValidation($validator->errors()->messages()), 422);
        }
        if ($row = $model->create(request()->all())) {
            return response()->json(['message' => trans('app.Your message has been sent successfully')], 201);
        }
        return response()->json(['message' => trans('app.Your message failed to send')], 400);
    }

    public function postSubscribe(\App\Models\Contact $model) {
        $rules = [
            'email' => 'required|email',
        ];
        if (env('ENABLE_CAPTCHA') == 1 && app()->environment() == 'production') {
            $rules['g-recaptcha-response'] = 'required';
        }
        $validator = \Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            return response()->json(transformValidation($validator->errors()->messages()), 422);
        }
        $contactExist = $model->where('email', request('email'))->first();
        if ($contactExist) {
            return response()->json(['message' => trans('app.You have successfully subscribed')], 200);
        } else {
            if ($row = $model->create(request()->all())) {
                return response()->json(['message' => trans('app.You have successfully subscribed')], 201);
            }
        }
        return response()->json(['message' => trans('app.You failed to subscribe')], 400);
    }

}
