<?php

namespace App\Http\Controllers;

class HomeController extends Controller {

    public $model;
    public $module;

    public function __construct() {
        $this->module = 'home';
    }

    public function getIndex() {
        $data['page_title'] = trans('app.Home');
        $data['slides']=\App\Models\Slide::active()->inRandomOrder()->limit(5)->get();
        return view('front.' . $this->module . '.index', $data);
    }

    public function postContact() {
        
    }

}
