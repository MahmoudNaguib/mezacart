<?php

namespace App\Http\Controllers;

class PagesController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Page $model) {
        $this->module = 'pages';
        $this->model = $model;
        $this->title = trans('app.Pages');
    }

    public function getIndex() {
        $data['page_title'] = $this->title;
        $data['meta_description'] = conf('application_name') . ' : ' . $this->title;
        $data['meta_keywords'] = conf('application_name') . ',' . $this->title;
        $data['rows'] = $this->model->filterAndSort()->active()->orderBy('created_at', 'DESC')->paginate(12);
        return view('front.' . $this->module . '.index', $data);
    }

    public function getDetails($id, $slug = NULL) {
        $data['row'] = $this->model->filterAndSort()->active()->findOrFail($id);
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['otherRows'] = $this->model->filterAndSort()->where('id', '!=', $id)->active()->inRandomOrder()->limit(5)->get();
        $data['page_title'] = $data['row']->title;
        $data['meta_description'] = ($data['row']->meta_description) ?: $data['row']->content_limited;
        $data['meta_keywords'] = $data['row']->meta_keywords;
        $data['image'] = $data['row']->image;
        /////////////////// update views
        $data['row']->increment('views');
        return view('front.' . $this->module . '.details', $data);
    }
     public function contact() {
        $data['page_title'] = trans('app.Contact us');
        $data['meta_description'] =  trans('app.Contact us');
        $data['meta_keywords'] =  trans('app.Contact us');
        $data['breadcrumb'] = [$this->title => $this->module];
        return view('front.' . $this->module . '.contact', $data);
    }
}
