<?php

namespace App\Http\Controllers;

class PostsController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Post $model) {
        $this->module = 'posts';
        $this->model = $model;
        $this->title = trans('app.Posts');
    }

    public function getIndex() {
        $data['page_title'] = $this->title;
        $data['meta_description'] = conf('application_name') . ' : ' . $this->title;
        $data['meta_keywords'] = conf('application_name') . ',' . $this->title;
        $data['rows'] = $this->model->filterAndSort()->active()->orderBy('created_at', 'DESC')->paginate(9);
        return view('front.' . $this->module . '.index', $data);
    }

    public function getDetails($id, $slug = NULL) {
        $data['row'] = $this->model->filterAndSort()->active()->findOrFail($id);
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['otherRows'] = $this->model->filterAndSort()->where('id', '!=', $id)->active()->inRandomOrder()->limit(5)->get();
        $data['page_title'] = $data['row']->title;
        $data['meta_description'] = ($data['row']->meta_description) ?: $data['row']->content_limited;
        $data['meta_keywords'] = $data['row']->meta_keywords;
        $data['image'] = $data['row']->image;
        /////////////////// update views
        $data['row']->increment('views');
        return view('front.' . $this->module . '.details', $data);
    }

}
