<?php

use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('posts')->delete();
        if (app()->environment() != 'testing') {
            DB::statement("ALTER TABLE posts AUTO_INCREMENT = 1");
        }
        factory(App\Models\Post::class, 5)->create();
    }

}
