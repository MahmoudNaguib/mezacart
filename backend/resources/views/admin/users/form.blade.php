@include('form.input',['name'=>'name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Name'),'placeholder'=>trans('admin.Name'),'required'=>1]])

@include('form.input',['name'=>'email','type'=>'email','attributes'=>['class'=>'form-control','label'=>trans('admin.Email'),'placeholder'=>trans('admin.Email'),'required'=>1]])

@include('form.input',['name'=>'mobile','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Mobile'),'placeholder'=>trans('admin.Mobile'),'required'=>1]])

@php 
    $attributes=['class'=>'form-control','label'=>trans('admin.Password'),'placeholder'=>trans('admin.Password'),'required'=>1];
    if(@$row->id) unset($attributes['required']);
@endphp

@include('form.password',['name'=>'password','attributes'=>$attributes])

@php 
    $attributes=['class'=>'form-control','label'=>trans('admin.Password confirmation'),'placeholder'=>trans('admin.Password confirmation'),'required'=>1];
    if(@$row->id) unset($attributes['required']);
@endphp
@include('form.password',['name'=>'password_confirmation','attributes'=>$attributes])

@include('form.select',['name'=>'country_id','options'=>$row->getCountries(),'attributes'=>['class'=>'form-control','label'=>trans('admin.Country'),'placeholder'=>trans('admin.Country'),'required'=>1]])

 @include('form.select',['name'=>'currency_id','options'=>$row->getCurrencies(),'attributes'=>['class'=>'form-control','label'=>trans('admin.Default currency'),'placeholder'=>trans('admin.Default currency'),'required'=>1]])

@include('form.select',['name'=>'language','options'=>languages(),'attributes'=>['class'=>'form-control','label'=>trans('admin.Default language'),'placeholder'=>trans('admin.Default language'),'required'=>1]])

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('admin.Avatar'),'placeholder'=>trans('admin.Avatar')]])


<h3>{{trans('admin.Admin Roles')}}</h3>
@include('form.select',['name'=>'role_id','options'=>$row->getRoles(),'attributes'=>['class'=>'form-control','label'=>trans('admin.Role'),'placeholder'=>trans('admin.Role')]])
