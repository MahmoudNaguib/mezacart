@extends('layouts.admin')
@section('title')
<h6 class="slim-pagetitle"> {{ trans('admin.Welcome') .', '.auth()->user()->name }}</h6>
@endsection

@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-3">
        <div class="card card-status">
            <div class="media">
                <i class="icon ion-ios-people-outline tx-purple"></i>
                <div class="media-body">
                    <h1>{{@$total_contacts}}</h1>
                    <p>{{ trans('admin.Total contacts')}}</p>
                </div><!-- media-body -->
            </div><!-- media -->
        </div><!-- card -->
    </div><!-- col-3 -->
    <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
        <div class="card card-status">
            <div class="media">
                <i class="icon ion-ios-contact-outline tx-primary"></i>
                <div class="media-body">
                    <h1>{{@$total_messages}}</h1>
                    <p>{{ trans('admin.Total messages')}}</p>
                </div><!-- media-body -->
            </div><!-- media -->
        </div><!-- card -->
    </div><!-- col-3 -->
    <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
        <div class="card card-status">
            <div class="media">
                <i class="icon ion-ios-cart-outline  tx-teal"></i>
                <div class="media-body">
                    <h1>{{@$total_posts}}</h1>
                    <p>{{ trans('admin.Total posts')}}</p>
                </div><!-- media-body -->
            </div><!-- media -->
        </div><!-- card -->
    </div><!-- col-3 -->
</div><!-- row -->
<div class="row row-xs">
    @if( can('view-messages'))
    <div class="col-md-6 mg-t-15">
        <div class="card card-table">
            <div class="card-header">
                <h6 class="slim-card-title">{{ trans('admin.Latest Messages')}}</h6>
            </div><!-- card-header -->
            <div class="table-responsive">
                <table class="table mg-b-0 tx-13">
                    <thead>
                        <tr class="tx-10">
                            <th class="pd-y-15">{{ trans('admin.Name')}}</th>
                            <th class="pd-y-15">{{ trans('admin.Email')}}</th>
                            <th class="pd-y-15">{{ trans('admin.Content')}}</th>
                            <th class="pd-y-10">{{ trans('admin.Created at')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$messages->isEmpty())
                        @foreach($messages as $row)
                        <tr>
                            <td class="valign-middle">
                                {{$row->name}}
                            </td>
                            <td class="valign-middle">{{$row->email}}</td>
                            <td class="valign-middle"><a href="messages/view/{{$row->id}}" class=" tx-14 tx-medium d-block ">{{str_limit(strip_tags($row->content,30))}}</a></td>
                            <td class="valign-middle">{{ $row->created_at}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!-- table-responsive -->
            <div class="card-footer tx-12 pd-y-15 bg-transparent">
                <a href="{{lang()}}/messages"><i class="fa fa-angle-down mg-r-5"></i>{{trans('admin.View all messages')}}</a>
            </div>

        </div><!-- card -->
    </div><!-- col-6 -->
    @endif
</div><!-- row -->
@endsection
