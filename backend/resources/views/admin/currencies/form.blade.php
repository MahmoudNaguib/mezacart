@foreach(langs() as $lang)
    @include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Title').' '.$lang,'placeholder'=>trans('admin.Title'),'required'=>1]])
@endforeach
@include('form.input',['name'=>'iso','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.ISO'),'placeholder'=>trans('admin.ISO'),'required'=>1]])

@include('form.input',['name'=>'rate','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('admin.Rate'),'placeholder'=>trans('admin.Rate'),'step'=>'0.01','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,3})?$']])
