<h2 class="slim-logo">
    <a href="{{app()->make("url")->to('/')}}/">
        <img src="{{uploads()}}/small/{{conf('logo')}}" alt="{{trans('admin.Logo')}}" height="50">
    </a>
</h2>