@if(auth()->user())
<div class="dropdown dropdown-c">
    <a href="#" class="logged-user" data-toggle="dropdown">
        {!!image(auth()->user()->image,'small') !!}
        <span>{{ auth()->user()->name }}</span>
        <i class="fa fa-angle-down"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <nav class="nav">
            @if(@auth()->user()->role_id)
            <a href="{{lang()}}/admin/dashboard" class="nav-link"><i class="icon ion-person"></i> {{ trans('admin.Admin Dashbaord') }}</a>
            @endif
            <a href="{{lang()}}/profile/edit" class="nav-link"><i class="icon ion-person"></i> {{ trans('admin.Edit account') }}</a>
            <a href="{{lang()}}/profile/change-password" class="nav-link"><i class="icon ion-person"></i> {{ trans('admin.Change password') }}</a>
            <a href="{{lang()}}/profile/logout" class="nav-link"><i class="icon ion-forward"></i>{{ trans('admin.Logout') }}</a>
        </nav>
    </div>
    <!-- dropdown-menu -->
</div>
<!-- dropdown -->
@else
<div class="dropdown dropdown-c">
    <a href="{{lang()}}/auth/login" class="logged-user" >
        <span>{{trans('admin.Login')}}</span>
        <i class="fa fa-angle-down"></i>
    </a>
</div>
@endif