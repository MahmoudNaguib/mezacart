@if(!auth()->guest())
<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is('*/admin/dashboard*'))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}/admin/dashboard">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('admin.Dashboard')}}</span>
                </a>
            </li>

            @if(can('create-pages') || can('view-pages'))
            <li class="nav-item {{(request()->is('*/admin/pages*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/pages">
                    <i class="icon ion-ios-compose-outline"></i>
                    <span>{{trans('admin.Pages')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-posts') || can('view-posts'))
            <li class="nav-item {{(request()->is('*/admin/posts*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/posts">
                    <i class="icon ion-ios-compose-outline"></i>
                    <span>{{trans('admin.Posts')}}</span>
                </a>
            </li>
            @endif
            @if(can('create-slides') || can('view-slides'))
            <li class="nav-item {{(request()->is('*/admin/slides*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/slides">
                    <i class="icon ion-ios-camera-outline"></i>
                    <span>{{trans('admin.Slides')}}</span>
                </a>
            </li>
            @endif
            @if(can('create-users') || can('view-users'))
            <li class="nav-item {{(request()->is('*/admin/users*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/users">
                    <i class="icon ion-ios-contact"></i>
                    <span>{{trans('admin.Users')}}</span>
                </a>
            </li>
            @endif
            <li class="nav-item with-sub {{(request()->is('*/admin/sections*') || request()->is('*/admin/products*') || request()->is('*/admin/orders*'))?"active":""}}">
                <a class="nav-link" href="#" data-toggle="dropdown">
                    <i class="icon ion-ios-cart-outline"></i>
                    <span>{{trans('admin.Catalog')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(can('create-sections') || can('view-sections'))
                        <li class="{{(request()->is('*/admin/sections*'))?"active":""}}"><a href="{{lang()}}/admin/sections">{{trans('admin.Section')}}</a>
                        </li>
                        @endif
                        @if(can('create-products') || can('view-products'))
                        <li class="{{(request()->is('*/admin/products*'))?"active":""}}"><a href="{{lang()}}/admin/products">{{trans('admin.Products')}}</a>
                        </li>
                        @endif
                        @if(can('view-orders'))
                        <li class="{{(request()->is('*/admin/orders*'))?"active":""}}"><a href="{{lang()}}/admin/orders">{{trans('admin.Orders')}}</a>
                        </li>
                        @endif
                    </ul>
                </div>
            </li>

            <li class="nav-item with-sub {{(request()->is('*/admin/roles*') || request()->is('*/admin/configs*') || request()->is('*/admin/countries*') || request()->is('*/admin/currencies*') || request()->is('*/admin/messages*') || request()->is('*/admin/contacts*'))?"active":""}}">
                <a class="nav-link" href="#" data-toggle="dropdown">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>{{trans('admin.Others')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(@auth()->user()->role_id==1)
                        <li class="{{(request()->is('*/admin/roles*'))?"active":""}}"><a href="{{lang()}}/admin/roles">{{trans('admin.Roles')}}</a>
                        </li>
                        @endif
                        @if(can('edit-configs'))
                        <li class="{{(request()->is('*/admin/admin/configs*'))?"active":""}}"><a href="{{lang()}}/admin/configs/edit">{{trans('admin.Configurations')}}</a>
                        </li>
                        @endif

                        @if(can('create-countries') || can('view-countries'))
                        <li class="{{(request()->is('*/admin/countries*'))?"active":""}}"><a href="{{lang()}}/admin/countries">{{trans('admin.Countries')}}</a>
                        </li>
                        @endif

                        @if(can('create-currencies') || can('view-currencies'))
                        <li class="{{(request()->is('*/admin/currencies*'))?"active":""}}"><a href="{{lang()}}/admin/currencies">{{trans('admin.Currencies')}}</a>
                        </li>
                        @endif
                        
                        @if(can('create-vouchers') || can('view-vouchers'))
                        <li class="{{(request()->is('*/admin/vouchers*'))?"active":""}}"><a href="{{lang()}}/admin/vouchers">{{trans('admin.Vouchers')}}</a>
                        </li>
                        @endif

                        @if(can('view-messages'))
                        <li class="{{(request()->is('*/admin/messages*'))?"active":""}}"><a href="{{lang()}}/admin/messages">{{trans('admin.Contact us messages')}}</a>
                        </li>
                        @endif

                        @if(can('view-contacts'))
                        <li class="{{(request()->is('*/admin/contacts*'))?"active":""}}"><a href="{{lang()}}/admin/contacts">{{trans('admin.Newsletter contacts')}}</a>
                        </li>
                        @endif

                    </ul>
                </div>
                <!-- dropdown-menu -->
            </li>

        </ul>
    </div>
    <!-- container -->
</div>
@endif