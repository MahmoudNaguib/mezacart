@foreach(langs() as $lang)
@php
$attributes=['class'=>'form-control','label'=>trans('admin.Title').' '.$lang,'placeholder'=>trans('admin.Title')];
if($lang=='en')
$attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
$attributes=['class'=>'form-control','label'=>trans('admin.Summary').' '.$lang,'placeholder'=>trans('admin.Summary')];
if($lang=='en')
$attributes['required']=1;
@endphp
@include('form.input',['name'=>'summary['.$lang.']','value'=>$row->getTranslation('summary',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
$attributes=['class'=>'form-control editor','label'=>trans('admin.Content').' '.$lang,'placeholder'=>trans('admin.Content')];
if($lang=='en')
$attributes['required']=1;
@endphp
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach

@foreach(langs() as $lang)
@include('form.input',['name'=>'tags['.$lang.']','value'=>$row->getTranslation('tags',$lang),'type'=>'text','attributes'=>['class'=>'form-control tags','label'=>trans('admin.Tags').' '.$lang,'placeholder'=>trans('admin.Tags')]])
@endforeach

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('admin.Image'),'placeholder'=>trans('admin.Image')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('admin.Is active')]])

<h3><u>{{trans('admin.Search engine optmization')}}(SEO)</u></h3>
@foreach(langs() as $lang)
@include('form.input',['name'=>'meta_description['.$lang.']','value'=>$row->getTranslation('meta_description',$lang),'type'=>'textarea','attributes'=>['class'=>'form-control','label'=>trans('admin.Meta description').' '.$lang,'placeholder'=>trans('admin.Meta description')]])
@endforeach

@foreach(langs() as $lang)
@include('form.input',['name'=>'meta_keywords['.$lang.']','value'=>$row->getTranslation('meta_keywords',$lang),'type'=>'text','attributes'=>['class'=>'form-control tags','label'=>trans('admin.Meta keywords').' '.$lang,'placeholder'=>trans('admin.Meta keywords')]])
@endforeach

@push('js')
<script>
    $(function () {
    $('form').submit(function () {
    var title_en = $('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
            if ($('input[name="title[{{$lang}}]"]').val() == '')
            $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach

            var content_en = $('textarea[name="content[en]"]').val();
            @foreach(langs() as $lang)
            if ($('textarea[name="content[{{$lang}}]"]').val() == '')
            $('textarea[name="content[{{$lang}}]"]').val(content_en);
            @endforeach

            var summary_en = $('textarea[name="summary[en]"]').val();
            @foreach(langs() as $lang)
            if ($('textarea[name="summary[{{$lang}}]"]').val() == '')
            $('textarea[name="summary[{{$lang}}]"]').val(content_en);
            @endforeach

            var meta_description_en = $('textarea[name="meta_description[en]"]').val();
            @foreach(langs() as $lang)
            if ($('textarea[name="meta_description[{{$lang}}]"]').val() == '')
            $('textarea[name="meta_description[{{$lang}}]"]').val(meta_description_en);
            @endforeach

            var meta_keywords_en = $('input[name="meta_keywords[en]"]').val();
            @foreach(langs() as $lang)
            if ($('input[name="meta_keywords[{{$lang}}]"]').val() == '')
            $('input[name="meta_keywords[{{$lang}}]"]').val(meta_keywords_en);
            @endforeach

    });
    });
</script>
@endpush
