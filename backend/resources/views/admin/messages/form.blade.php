
@include('form.input',['name'=>'name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Name'),'placeholder'=>trans('admin.Name'),'required'=>1]])
@include('form.input',['name'=>'email','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Email'),'placeholder'=>trans('admin.Email'),'required'=>1]])
@include('form.input',['name'=>'content','type'=>'textarea','attributes'=>['class'=>'form-control editor','label'=>trans('admin.Content'),'placeholder'=>trans('admin.Content'),'required'=>1]])
