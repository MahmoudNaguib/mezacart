@extends('layouts.admin')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(request('q'))
    @if(isset($pages) && !$pages->isEmpty())
    <h4>{{trans('admin.Pages')}}</h4>
    <ul>
        @foreach($pages as $row)
        <li><a href="admin/pages/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($gallery) && !$gallery->isEmpty())
    <h4>{{trans('admin.Gallery')}}</h4>
    <ul>
        @foreach($gallery as $row)
        <li><a href="admin/gallery/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($products) && !$products->isEmpty())
    <h4>{{trans('admin.Products')}}</h4>
    <ul>
        @foreach($products as $row)
        <li><a href="admin/products/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($services) && !$services->isEmpty())
    <h4>{{trans('admin.Services')}}</h4>
    <ul>
        @foreach($services as $row)
        <li><a href="admin/services/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($posts) && !$posts->isEmpty())
    <h4>{{trans('admin.Posts')}}</h4>
    <ul>
        @foreach($posts as $row)
        <li><a href="admin/posts/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($why) && !$why->isEmpty())
    <h4>{{trans('admin.Why')}}</h4>
    <ul>
        @foreach($why as $row)
        <li><a href="admin/why/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($users) && !$users->isEmpty())
    <h4>{{trans('admin.Users')}}</h4>
    <ul>
        @foreach($users as $row)
        <li><a href="admin/users/view/{{$row->id}}" target="_blank">{{$row->name}}, {{$row->email}}, {{$row->mobile}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($messages) && !$messages->isEmpty())
    <h4>{{trans('admin.Messages')}}</h4>
    <ul>
        @foreach($messages as $row)
        <li><a href="admin/messages/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif
    @if(isset($contacts) && !$contacts->isEmpty())
    <h4>{{trans('admin.Contacts')}}</h4>
    <ul>
        @foreach($contacts as $row)
        <li><a href="admin/contacts/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif

    @endif
</div>
@endsection