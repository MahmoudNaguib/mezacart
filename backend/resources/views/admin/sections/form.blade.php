@include('form.select',['name'=>'top_id','options'=>$row->getMainSections(),'attributes'=>['class'=>'form-control','label'=>trans('admin.Parent section'),'placeholder'=>trans('admin.Parent section')]])

@foreach(langs() as $lang)
    @include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Title').' '.$lang,'placeholder'=>trans('admin.Title'),'required'=>1]])
@endforeach


@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('admin.Image'),'placeholder'=>trans('admin.Image')]])


@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('admin.Is active')]])