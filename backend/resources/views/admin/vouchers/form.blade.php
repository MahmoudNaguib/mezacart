
@include('form.input',['name'=>'code','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('admin.Code'),'placeholder'=>trans('admin.Code'),'text'=>trans('admin.If code not set it will be generated')]])

@include('form.select',['name'=>'currency_id','options'=>$row->getCurrencies(),'attributes'=>['class'=>'form-control','label'=>trans('admin.Currency'),'placeholder'=>trans('admin.Currency'),'required'=>1]])

@include('form.input',['name'=>'value','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('admin.Value'),'placeholder'=>trans('admin.Value'),'step'=>'0.01','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,3})?$']])


@include('form.input',['name'=>'expiry_date','type'=>'text','attributes'=>['class'=>'form-control datepicker','label'=>trans('admin.Expiry date'),'placeholder'=>trans('admin.Expiry date')]])

@include('form.input',['name'=>'max_usage','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('admin.Max Usage'),'placeholder'=>trans('admin.Max Usage'),'step'=>'1','required'=>1,'min'=>0]])
