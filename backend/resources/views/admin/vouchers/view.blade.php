@extends('layouts.admin')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{lang()}}/admin/{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('admin.Edit')}}
    </a><br>
    @endif
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('admin.Code')}}</td>
                <td width="75%" class="align-left">{{@$row->code}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('admin.Value')}}</td>
                <td width="75%" class="align-left">{{$row->value}} {{$row->currency->iso}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('admin.Expiry date')}}</td>
                <td width="75%" class="align-left">{{$row->expiry_date}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('admin.Max Usage')}}</td>
                <td width="75%" class="align-left">{{$row->max_usage}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('admin.Used times')}}</td>
                <td width="75%" class="align-left">{{$row->used}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('admin.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>
            
        </table>
    </div>
</div>
@endsection
