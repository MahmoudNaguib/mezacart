@extends('layouts.admin')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('create-'.$module))
    <a href="{{lang()}}/admin/{{$module}}/create" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('admin.Create')}}
    </a>
    @endif
    @if(can('view-'.$module))
    <a href="{{lang()}}/admin/{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-download"></i> {{trans('admin.Export')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('admin.ID')}} </th>
                    <th class="wd-10p">{{trans('admin.Code')}} </th>
                    <th class="wd-10p">{{trans('admin.Value')}} </th>
                    <th class="wd-10p">{{trans('admin.Expiry date')}} </th>
                    <th class="wd-10p">{{trans('admin.Max Usage')}} </th>
                    <th class="wd-10p">{{trans('admin.Used times')}} </th>
                    <th class="wd-15p">{{trans('admin.Created at')}}</th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->code}}</td>
                    <td class="center">{{$row->value}} {{$row->currency->iso}}</td>
                    <td class="center">{{$row->expiry_date}}</td>
                    <td class="center">{{$row->max_usage}}</td>
                    <td class="center">{{$row->used}}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        @if(can('edit-'.$module))
                        <a class="btn btn-success btn-xs" href="{{lang()}}/admin/{{$module}}/edit/{{$row->id}}" title="{{trans('admin.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endif

                        @if(can('view-'.$module))
                        <a class="btn btn-primary btn-xs" href="{{lang()}}/admin/{{$module}}/view/{{$row->id}}" title="{{trans('admin.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$module))
                        <a class="btn btn-danger btn-xs" href="{{lang()}}/admin/{{$module}}/delete/{{$row->id}}" title="{{trans('admin.Delete')}}" data-confirm="{{trans('admin.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends([])->render() !!}
    </div>
    @else
    {{trans("admin.There is no results")}}
    @endif
    @endif
</div>
@endsection
