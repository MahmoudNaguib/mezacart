@extends('layouts.front')
@section('content')
@include('front.partials.title',['page_title'=>trans('app..We Are Sorry Some thing wrong')])
@include('front.partials.breadcrumb')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h3 class="signin-title-secondary">
                {{trans('app.Click the home page to navigate')}}
            </h3>
            <h3 class="signin-title-secondary">
                <a href="{{app()->make("url")->to('/')}}">{{trans('app.Home')}}</a>
            </h3>
            <h3 class="signin-title-secondary">
                <a href="{{app()->make("url")->to('/')}}">{{trans('app.Contact')}}</a>
            </h3>
        </div>
        <div class="col-md-6 text-right">
            <img class="img-fluid" src="front/images/404.gif" alt="404">
        </div>
    </div>
</div>

@endsection

