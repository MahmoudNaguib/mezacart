<!DOCTYPE html>
<html dir="{{(lang()=='en'?'ltr':'rtl')}}" lang="{{lang()}}">
    <head>
        @include('front.partials.meta')
        @include('front.partials.css')
        @stack('css')
    </head>
    <body class="{{(lang()=='ar')?'rtl':''}}" data-spy="scroll" data-target=".navbar" data-offset="75">
        <!-- loader-->
        <div class="loader-wrapper">
            <div id="loader">
                <div id="shadow"></div>
                <div id="box"></div>
            </div>
        </div>
        <!-- loader end-->
        <!-- Nav start-->
        @include('front.partials.navigation')
        @include('front.partials.flash_messages')
        <!-- section-wrapper -->
        @yield('content')

        @include('front.partials.footer')
        <div class="tap-top">
            <div><i class="fa fa-angle-up" aria-hidden="true"></i></div>
        </div>
        @include('front.partials.js')
        @stack('js')
    </body>
</html>