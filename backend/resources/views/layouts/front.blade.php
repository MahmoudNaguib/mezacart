<!doctype html>
<html class="no-js" dir="{{(lang()=='en'?'ltr':'rtl')}}" lang="{{lang()}}">

    <head>
        @include('front.partials.head')
    </head>

    <body class="template-color-2 font-family-01">

        <div class="main-wrapper">

            <!-- Begin Loading Area -->
            @include('front.partials.loader')
            <!-- Loading Area End Here -->

            <!-- Begin Main Header Area -->
            <header class="main-header_area">
                @include('front.partials.header') 
            </header>
            <!-- Main Header Area End Here -->

            <!-- Main Header Area End Here -->
            @include('front.partials.flash_messages')
            <!-- section-wrapper -->
            @yield('content')

            <!-- Begin Footer Area -->
            <div class="footer-area">
                @include('front.partials.footer')
            </div>
            <!-- Footer Area End Here -->

            <!-- Scroll To Top Start -->
            <a class="scroll-to-top" href=""><i class="icon-arrow-up"></i></a>
            <!-- Scroll To Top End -->

        </div>
        @include('front.partials.js')


    </body>

</html>