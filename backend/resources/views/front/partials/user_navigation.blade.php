@if(auth()->check())
<li class="position-relative">
    <a href="javascript:void(0)">{{trans('app.Welcome')}} {{auth()->user()->name}}</a>
</li>
<li class="position-relative">
    <a href="{{lang()}}/profile/change-password">{{trans('app.Change password')}}</a>
</li>
<li class="position-relative">
    <a href="{{lang()}}/profile/edit">{{trans('app.Edit profile')}}</a>
</li>
<li class="position-relative">
    <a href="{{lang()}}/profile/logout">{{trans('app.Logout')}}</a>
</li>
@else
<li class="position-relative">
    <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a>
</li>
<li class="position-relative">
    <a href="{{lang()}}/auth/register">{{trans('app.Register')}}</a>
</li>
@endif