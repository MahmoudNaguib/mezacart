<!-- Begin Slider Area Two -->
@if($slides)
<div class="slider-area slider-area-2">
    <div class="quicky-element-carousel home-slider home-slider-2 arrow-style" data-slick-options='{
         "slidesToShow": 1,
         "slidesToScroll": 1,
         "infinite": true,
         "arrows": false,
         "dots": true,
         "autoplay" : true,
         "autoplaySpeed" : 7000,
         "pauseOnHover" : false,
         "pauseOnFocus" : false
         }' data-slick-responsive='[
         {"breakpoint":768, "settings": {
         "slidesToShow": 1
         }},
         {"breakpoint":575, "settings": {
         "slidesToShow": 1
         }}
         ]'>
         @foreach($slides as $row)
        <div class="slide-item position-relative bg-3" style="background-image: url({{uploads()}}/large/{{$row->image}});">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-slide">
                            <div class="slide-content">
                                <h2 class="freestyle-script">{{$row->title}}</h2>
                                <p class="short-desc">
                                    {{$row->content}}
                                </p>
                                <div class="slide-btn">
                                    <a class="quicky-btn horizontal-line_ltr" href="{{$row->url}}">{{trans('app.More')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         @endforeach

    </div>

</div>
@endif
<!-- Slider Area Two End Here -->