<!-- Begin Quicky's Modal Area -->
<div class="modal fade modal-wrapper" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-inner-area sp-area row">
                    <div class="col-xl-5 col-lg-6">
                        <div class="sp-img_area">
                            <div class="quicky-element-carousel sp-img_slider slick-img-slider" data-slick-options='{
                                 "slidesToShow": 1,
                                 "arrows": false,
                                 "fade": true,
                                 "draggable": false,
                                 "swipe": false,
                                 "asNavFor": ".sp-img_slider-nav"
                                 }'>
                                <div class="single-slide red">
                                    <img src="assets/images/product/large-size/1.jpg" alt="Quicky's Product Image">
                                </div>
                                <div class="single-slide orange">
                                    <img src="assets/images/product/large-size/2.jpg" alt="Quicky's Product Image">
                                </div>
                                <div class="single-slide brown">
                                    <img src="assets/images/product/large-size/3.jpg" alt="Quicky's Product Image">
                                </div>
                                <div class="single-slide umber">
                                    <img src="assets/images/product/large-size/4.jpg" alt="Quicky's Product Image">
                                </div>
                            </div>
                            <div class="quicky-element-carousel sp-img_slider-nav arrow-style arrow-sm_size arrow-day_color" data-slick-options='{
                                 "slidesToShow": 3,
                                 "asNavFor": ".sp-img_slider",
                                 "focusOnSelect": true,
                                 "arrows" : true,
                                 "spaceBetween": 30
                                 }' data-slick-responsive='[
                                 {"breakpoint":1501, "settings": {"slidesToShow": 3}},
                                 {"breakpoint":1200, "settings": {"slidesToShow": 2}},
                                 {"breakpoint":992, "settings": {"slidesToShow": 3}},
                                 {"breakpoint":768, "settings": {"slidesToShow": 3}},
                                 {"breakpoint":575, "settings": {"slidesToShow": 2}}
                                 ]'>
                                <div class="single-slide red">
                                    <img src="assets/images/product/large-size/1.jpg" alt="Quicky's Product Thumnail">
                                </div>
                                <div class="single-slide orange">
                                    <img src="assets/images/product/large-size/2.jpg" alt="Quicky's Product Thumnail">
                                </div>
                                <div class="single-slide brown">
                                    <img src="assets/images/product/large-size/3.jpg" alt="Quicky's Product Thumnail">
                                </div>
                                <div class="single-slide umber">
                                    <img src="assets/images/product/large-size/4.jpg" alt="Quicky's Product Thumnail">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6">
                        <div class="sp-content">
                            <div class="sp-heading">
                                <h5><a href="#">Moonstar Clock</a></h5>
                            </div>
                            <div class="rating-box">
                                <ul>
                                    <li><i class="zmdi zmdi-star"></i></li>
                                    <li><i class="zmdi zmdi-star"></i></li>
                                    <li><i class="zmdi zmdi-star"></i></li>
                                    <li class="silver-color"><i class="zmdi zmdi-star"></i></li>
                                    <li class="silver-color"><i class="zmdi zmdi-star"></i></li>
                                </ul>
                            </div>
                            <div class="price-box">
                                <span class="new-price new-price-2 ml-0">$194.00</span>
                                <span class="old-price">$241.00</span>
                            </div>
                            <div class="sp-essential_stuff">
                                <ul>
                                    <li>Brands <a href="javascript:void(0)">Buxton</a></li>
                                    <li>Product Code: <a href="javascript:void(0)">Product 16</a></li>
                                    <li>Reward Points: <a href="javascript:void(0)">100</a></li>
                                    <li>Availability: <a href="javascript:void(0)">In Stock</a></li>
                                    <li>EX Tax: <a href="javascript:void(0)"><span>$453.35</span></a></li>
                                    <li>Price in reward points: <a href="javascript:void(0)">400</a></li>
                                </ul>
                            </div>
                            <div class="color-list_area">
                                <div class="color-list_heading">
                                    <h4>Available Options</h4>
                                </div>
                                <span class="sub-title">Color</span>
                                <div class="color-list">
                                    <a href="javascript:void(0)" class="single-color active" data-swatch-color="red">
                                        <span class="bg-red_color"></span>
                                        <span class="color-text">Red (+$150)</span>
                                    </a>
                                    <a href="javascript:void(0)" class="single-color" data-swatch-color="orange">
                                        <span class="burnt-orange_color"></span>
                                        <span class="color-text">Orange (+$170)</span>
                                    </a>
                                    <a href="javascript:void(0)" class="single-color" data-swatch-color="brown">
                                        <span class="brown_color"></span>
                                        <span class="color-text">Brown (+$120)</span>
                                    </a>
                                    <a href="javascript:void(0)" class="single-color" data-swatch-color="umber">
                                        <span class="raw-umber_color"></span>
                                        <span class="color-text">Umber (+$125)</span>
                                    </a>
                                </div>
                            </div>
                            <div class="quantity">
                                <label>Quantity</label>
                                <div class="cart-plus-minus">
                                    <input class="cart-plus-minus-box" value="1" type="text">
                                    <div class="dec qtybutton"><i class="zmdi zmdi-chevron-down"></i></div>
                                    <div class="inc qtybutton"><i class="zmdi zmdi-chevron-up"></i></div>
                                </div>
                            </div>
                            <div class="quicky-group_btn">
                                <ul>
                                    <li><a href="cart.html" class="add-to_cart">Add To Cart</a></li>
                                    <li><a href="wishlist.html"><i class="zmdi zmdi-favorite-outline"></i></a></li>
                                    <li><a href="compare.html"><i class="zmdi zmdi-shuffle"></i></a></li>
                                </ul>
                            </div>
                            <div class="quicky-tag-line">
                                <h6>Tags:</h6>
                                <a href="javascript:void(0)">clock,</a>
                                <a href="javascript:void(0)">watch,</a>
                                <a href="javascript:void(0)">bag</a>
                            </div>
                            <div class="quicky-social_link">
                                <ul>
                                    <li class="facebook">
                                        <a href="https://www.facebook.com" data-toggle="tooltip" target="_blank" title="Facebook">
                                            <i class="fab fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="twitter">
                                        <a href="https://twitter.com" data-toggle="tooltip" target="_blank" title="Twitter">
                                            <i class="fab fa-twitter-square"></i>
                                        </a>
                                    </li>
                                    <li class="youtube">
                                        <a href="https://www.youtube.com" data-toggle="tooltip" target="_blank" title="Youtube">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li class="google-plus">
                                        <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                            <i class="fab fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li class="instagram">
                                        <a href="https://www.instagram.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Quicky's Modal Area End Here -->