<!-- breadcrumb start-->
<div class="breadcrumb-bg">
    <div class="container">
        <div class="row">
            <nav class="theme-breadcrumb" aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0">
                    <li class="breadcrumb-item"><a href="{{app()->make("url")->to('/')}}/{{lang()}}">{{trans('app.Home')}}</a></li>
                    @if(@$breadcrumb) 
                    @foreach ($breadcrumb as $key=>$value)
                    <li class="breadcrumb-item"><a href="{{ $value }}">{{ $key }}</a></li>
                    @endforeach 
                    @endif
                    <li class="breadcrumb-item active" aria-current="page">{{ @$page_title }}</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- breadcrumb end-->
