<div class="main-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-header_nav position-relative">
                    <div class="header-logo_area">
                        @include('front.partials.logo')
                    </div>
                    <div class="main-menu_area d-none d-lg-block">
                        <nav class="main-nav d-flex justify-content-center">
                            <ul>
                                @include('front.partials.main_navigation')
                            </ul>
                        </nav>
                    </div>
                    <div class="header-right_area">
                        <ul>
                            <li class="mobile-menu_wrap d-inline-block d-lg-none">
                                <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color--white">
                                    <i class="zmdi zmdi-menu"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#searchBar" class="search-btn toolbar-btn">
                                    <i class="zmdi zmdi-search"></i>
                                </a>
                            </li>
                            <li class="minicart-wrap mr-md_0">
                                <a href="#miniCart" class="minicart-btn toolbar-btn">
                                    <div class="minicart-count_area">
                                        <i class="zmdi zmdi-mall"></i>
                                        <p class="total-price">$420 <span>(10)</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-holder user-setting_wrap">
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-account-o"></i>
                                </a>
                                <ul class="quicky-dropdown">
                                    @include('front.partials.user_navigation')
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('front.partials.cart')

<div class="mobile-menu_wrapper" id="mobileMenu">
    <div class="offcanvas-menu-inner">
        <div class="container">
            <a href="#" class="btn-close white-close_btn"><i class="zmdi zmdi-close"></i></a>
            <div class="offcanvas-inner_logo">
                @include('front.partials.logo')
            </div>
            <nav class="offcanvas-navigation">
                <ul class="mobile-menu">
                    @include('front.partials.main_navigation')
                </ul>
            </nav>
            <nav class="offcanvas-navigation user-setting_area">
                <ul class="mobile-menu">
                    <li class="menu-item-has-children active">
                        <a href="#">
                            <span class="mm-text">
                                {{trans('app.User account')}}
                            </span>
                        </a>
                        <ul class="sub-menu">
                            @include('front.partials.user_navigation')
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="offcanvas-search_wrapper" id="searchBar">
    <div class="offcanvas-menu-inner">
        <div class="container">
            <a href="#" class="btn-close"><i class="zmdi zmdi-close"></i></a>
            <!-- Begin Offcanvas Search Area -->
            <div class="offcanvas-search">
                <form action="search" class="hm-searchbox">
                    <input type="text" name="q" placeholder="{{trans('app.Search for item')}}">
                    <button class="search_btn" type="submit"><i
                            class="zmdi zmdi-search"></i></button>
                </form>
            </div>
            <!-- Offcanvas Search Area End Here -->
        </div>
    </div>
</div>
<div class="global-overlay"></div>