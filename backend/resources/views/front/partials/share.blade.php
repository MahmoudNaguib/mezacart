<div class="social-link-4">
    <ul>
        <li class="facebook">
            <a href="https://facebook.com/sharer/sharer.php?u={{$row->link}}" data-toggle="tooltip" target="_blank" title="" data-original-title="{{trans('app.Facebook')}}">
                <i class="zmdi zmdi-facebook"></i>
            </a>
        </li>
        <li class="twitter">
            <a href="https://twitter.com/intent/tweet?url={{$row->link}}" data-toggle="tooltip" target="_blank" title="" data-original-title="{{trans('app.Twitter')}}">
                <i class="zmdi zmdi-twitter"></i>
            </a>
        </li>
        <li class="linkedin">
            <a href="https://www.linkedin.com/shareArticle?url={{$row->link}}" data-toggle="tooltip" target="_blank" title="" data-original-title="{{trans('app.Linkedin')}}">
                <i class="zmdi zmdi-linkedin"></i>
            </a>
        </li>
        <li class="whatsapp">
            <a href="https://web.whatsapp.com/send?text={{$row->link}}" data-toggle="tooltip" target="_blank" title="" data-original-title="{{trans('app.Whatsapp')}}">
                <i class="zmdi zmdi-whatsapp"></i>
            </a>
        </li>
    </ul>
</div>