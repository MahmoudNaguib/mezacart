@extends('layouts.front')
@section('content')
@include('front.partials.title')
@include('front.partials.breadcrumb')
<div class="container">
    <div class="col-md-12 order-1 order-lg-2 pb-30">
        <div class="blog-item">
            <div class="col-8 offset-2">
                <div class="blog-img">
                    {!! image($row->image,'large',['class'=>'img-fluid','width'=>NULL,'alt'=>$row->title]) !!}
                </div>
            </div>
            <div class="blog-content">
                <h3 class="heading">
                    {{$row->title}}
                </h3>
                <div class="blog-meta">
                    <span>{{str_limit($row->created_at,10,false)}}, {{$row->views}} {{trans('app.Views')}}</span>
                </div>
            </div>
            <div class="blog-additional_information">
                <p>
                    {{$row->content}}
                </p>
            </div>
            @include('front.partials.share')
            @if($otherRows)
            <div class="related-post_area pt-xs-25">
                <h3 class="heading">{{trans('app.Related pages')}}</h3>
                <div class="row">
                    @foreach($otherRows as $row)
                    <div class="col-lg-3">
                        <div class="related-post_info">
                            <div class="related-post_img">
                                {!! image($row->image,'small',['class'=>'img-fluid','width'=>NULL,'alt'=>$row->title]) !!}
                            </div>
                            <div class="related-post_content">
                                <h3 class="title">{{$row->title_limited}}</h3>
                                <p class="short-desc">
                                    {{str_limit($row->created_at,10,false)}}, {{$row->views}} {{trans('app.Views')}}
                                </p>
                                <p class="short-desc">
                                    {{$row->content_limited}}.
                                </p>
                                <div class="readmore-ps_left">
                                    <a href="{{$row->link}}">{{trans('app.Read more')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@push('js')
@endpush
