@extends('layouts.front')
@section('content')
@include('front.partials.title')
@include('front.partials.breadcrumb')
<div class="blog-grid_area latest-blog_area-2 ptb-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!$rows->isEmpty())
                <div class="row blog-item_wrap">
                    @foreach($rows as $row)
                    <div class="col-lg-3 pb-30">
                        <div class="blog-item">
                            <div class="blog-img">
                                <a href="{{$row->link}}">
                                    {!! image($row->image,'small',['class'=>'img-fluid','width'=>NULL,'alt'=>$row->title]) !!}
                                </a>
                            </div>
                            <div class="blog-content bg-buttery-white">
                                <h3 class="heading titillium-font">
                                    <a href="{{$row->link}}">{{$row->title_limited}}</a>
                                </h3>
                                <p class="short-desc">
                                    {{str_limit($row->created_at,10,false)}}, {{$row->views}} {{trans('app.Views')}}
                                </p>
                                <p class="short-desc">
                                    {{$row->content_limited}}.
                                </p>
                                <div class="readmore-ps_left">
                                    <a href="{{$row->link}}">{{trans('app.Read more')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- paginations-->
                <div class="col-md-12 pb-30">
                    <div class="paganition-center pagination justify-content-center"> 
                        {!! $rows->appends(['created_by'=>request('created_by')])->render() !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
@endpush
