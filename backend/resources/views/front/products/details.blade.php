@extends('layouts.front')
@section('content')
<div class="page-margin">
    <!-- breadcrumb start-->
    <div class="breadcrumb-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="title"><span>{{$page_title}}</span></h2>
                </div>
                <div class="col-md-6 col-sm-6">
                    <nav class="theme-breadcrumb" aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent mb-0">
                            <li class="breadcrumb-item"><a href="{{app()->make("url")->to('/')}}/{{lang()}}">{{trans('app.Home')}}</a></li>

                            @if(@$breadcrumb) 
                            @foreach ($breadcrumb as $key=>$value)
                            <li class="breadcrumb-item"><a href="{{ $value }}">{{ $key }}</a></li>
                            @endforeach 
                            @endif
                            <li class="breadcrumb-item active" aria-current="page">{{ @$page_title }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end-->
    <!-- review Section start-->
    <!-- blog Section start-->
    <section class="blog-page">
        <div class="container">
            <div class="row">
                <!-- blog details starts-->
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="blog-details news-slid">
                        <div class="news-box">
                            {!! image($row->image,'large',['class'=>'img-fluid','width'=>NULL,'alt'=>$row->title]) !!}
                        </div>
                        <div class="news-text">
                            <div class="blog-hover">
                                <h4>{{$row->title}}</h4>
                                <ul class="list-inline blog-details-list">
                                    <li>{{str_limit($row->created_at,10,false)}}</li>
                                    <li>{{$row->views}} {{trans('app.Views')}}</li>
                                </ul>
                            </div>
                            <p>
                                {!! $row->content !!}
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 offset-md-1 leave-coment">
                            <div id="disqus_thread"></div>
                        </div>
                    </div>
                </div>
                <!-- blog details end-->
                <!--Blog sidebar  -->
                <div class="col-md-4 col-lg-3 order-md-first list-sidebar">
                    <div class="sidebar">
                        @if($otherRows)
                        <h4 class="blog-title">{{trans('app.Others')}}</h4>
                        <div class="blog-divider"></div>
                        <div class="recent-blog marg-20">
                            @foreach($otherRows as $row)
                            <div class="media">
                                {!! image($row->image,'small',['class'=>'mr-3','width'=>NULL,'alt'=>$row->title]) !!}
                                <div class="media-body"><a href="{{$row->link}}">
                                        <h5 class="mt-0">{{$row->title_limited}}</h5></a>
                                    <p class="m-0">{{str_limit($row->created_at,10,false)}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </div>
                <!--Blog sidebar Ends-->
            </div>
        </div>
    </section>
    <!-- review Section End-->
</div>


@endsection


@push('js')


@endpush