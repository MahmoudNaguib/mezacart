@extends('layouts.front')
@section('content')
<div class="page-margin">
    <!-- breadcrumb start-->
    <div class="breadcrumb-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 d-align-center">
                    <h2 class="title"><span>{{$page_title}}</span></h2>
                </div>
                <div class="col-md-6 col-sm-6">
                    <nav class="theme-breadcrumb" aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent mb-0">
                            <li class="breadcrumb-item"><a href="{{app()->make("url")->to('/')}}/{{lang()}}">{{trans('app.Home')}}</a></li>
                            <li class="breadcrumb-item active">{{$page_title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end-->
    <!-- blog Section start-->

    <section class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row blog-list">
                        @if(!$rows->isEmpty())
                        @foreach($rows as $row)
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="item news-slid">
                                <a href="{{$row->link}}">
                                    <div class="news-box">
                                        {!! image($row->image,'small',['class'=>'img-fluid','width'=>NULL,'alt'=>$row->title]) !!}
                                    </div></a>
                                <div class="news-text">
                                    <div class="blog-hover">
                                        <h4>{{$row->title_limited}}</h4>
                                        <ul class="list-inline blog-details-list">
                                            <li>{{str_limit($row->created_at,10,false)}}</li>
                                            <li>{{$row->views}} {{trans('app.Views')}}</li>
                                        </ul>
                                    </div>
                                    <p>
                                        {{$row->content_limited}}
                                    </p><a class="btn-theme" href="{{$row->link}}">{{trans('app.View more')}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        {{trans('app.There is no results')}}
                        @endif
                    </div>
                </div>
                 @if(!$rows->isEmpty())
                <!-- paginations-->
                <div class="col-md-12">
                    <div class="paganition-center pagination justify-content-center"> 
                            {!! $rows->appends(['created_by'=>request('created_by')])->render() !!}
                    </div>
                </div>
                @endif
                <!-- paginations end   -->
            </div>
        </div>
    </section>

</div>
</div>
@endsection