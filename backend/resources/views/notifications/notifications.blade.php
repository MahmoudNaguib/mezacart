@php 
$notifications = auth()->user()->notifications()->unreaded()->latest()->get();
@endphp

@if(@$notifications)
<!-- dropdown -->
<div class="dropdown dropdown-b">
    <a href="" class="header-notification" data-toggle="dropdown">
        <i class="icon ion-ios-bell-outline"></i>
        @if(!@$notifications->isEmpty())
        <span class="indicator">{{$notifications->count()}}</span>
        @endif
    </a>
    <div class="dropdown-menu">
        <div class="dropdown-menu-header">
            <h6 class="dropdown-menu-title">{{trans('app.Notifications')}}</h6>
            <div>
                <a href="notifications/read-all">{{trans('app.Mark all as Read')}}</a>
            </div>
        </div>
        <!-- dropdown-menu-header -->
        <div class="dropdown-list">
            @foreach(@$notifications as $notification)
            @if($loop->iteration<6)
            <!-- loop starts here -->
            <a href="notifications/to/{{$notification->id}}" class="dropdown-link">
                <div class="media">
                    <div class="media-body">
                        <p>
                            {{str_limit(strip_tags($notification->title),100)}}
                        </p>
                        <span>{{$notification->created_at}}</span>
                    </div>
                </div>
                <!-- media -->
            </a>
            <!-- loop ends here -->
            @endif
            @endforeach
            <div class="dropdown-list-footer">
                <a href="{{lang()}}/notifications"><i class="fa fa-angle-down"></i>{{trans('app.Show All')}}</a>
            </div>
        </div>
        <!-- dropdown-list -->
    </div>
    <!-- dropdown-menu-right -->
</div>
<!-- dropdown -->
@endif

