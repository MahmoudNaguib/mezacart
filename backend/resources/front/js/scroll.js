"use strict";
$(document).ready(function () {
    $("nav a").on('click', function (event) {
        if (this.hash !== "") {
            var hash = this.hash;
            if ($(hash).offset() != undefined) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 50
                }, 1000, function () {
                });
                return false;
            }
            return true;
        }
    });
});