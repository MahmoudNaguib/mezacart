/*-----------------------------------------------------------------------------------
 
 Template Name:Chatloop APP
 Template URI: themes.pixelstrap.com/chatloop
 Description: This is App Landing Page
 Author: Pixelstrap
 Author URI: https://themeforest.net/user/pixelstrap
 
 -----------------------------------------------------------------------------------*/
"use strict";
$(document).ready(function () {

    /*------------------------
     loader
     --------------------------*/
    $('.loader-wrapper').fadeOut('slow', function () {
        $(this).remove();
    });

    /*----------------------------------------
     mobile menu nav click hide collapse
     ----------------------------------------*/
    var mobile_menu = $(window).width();
    if (mobile_menu < 991) {
        $("nav a.nav-link").on('click', function (event) {
            if (!$(this).hasClass('dropdown-toggle')) {

                console.log('click');
                $(".navbar-collapse").collapse('hide');
            }

        });
    }

    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;
    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
        });
    }

    /*------------------------
     darkheader
     --------------------------*/
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 60) {
            $(".navbar").addClass("darkHeader");
        } else {
            $(".navbar").removeClass("darkHeader");
        }
    });

    /*------------------------
     Tap to top
     --------------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 600) {
            $('.tap-top').fadeIn();
        } else {
            $('.tap-top').fadeOut();
        }
    });
    $('.tap-top').on('click', function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /*-----------------------
     Team
     -------------------------*/
    $("#hide").on('click', function (event) {
        $("p").hide();
    });
    $("#show").on('click', function (event) {
        $("p").show();
    });
    $('.team-hover').hide();

    $(".team-under-box-button").on('click', function (event) {
        let el = $(this).attr('data-target');
        console.log('elem:' + el);
        $('.team-box').hide(1000);
        $(el + ' .team-hover').show(1000);
        $(el + ' .team-hover').fadeIn("slow");
    });
    $(".team-close-btn").on('click', function (event) {
        $(".team-hover").hide(1000);
        $('.team-box').show(1000);
        $('.team-box').fadeIn("slow");
    });


    $("#mymenu li a").on('click', function (event) {
        $('a.nav-link').removeClass('active');
        $(this).addClass('active');
    });
    //////////////////////////////////////////////////////////////// Plugins
    function confirmation() {
        $('a[data-confirm]').on('click', function () {
            var href = $(this).attr('href');
            var title = $(this).data("title");
            if (!$('#dataConfirmModal').length) {
                $('body').append('  <div class="modal fade" id="dataConfirmModal" tabindex="-1" role="document">\n\
						   <div class="modal-dialog modal-sm" role="document">\n\
						    <div class="modal-content bd-0">\n\
						      <div class="modal-body tx-center pd-y-20 pd-x-20">\n\
						        Hi there, I am a Modal Example for Dashio Admin Panel.\n\
						      </div>\n\
						      <div class="modal-footer justify-content-center">\n\
						        <button type="button" class="btn btn-master" data-dismiss="modal">Cancel</button>\n\
						        <a class="btn btn-danger" id="dataConfirmOK">Yes</a>\n\
						      </div>\n\
						    </div>\n\
						  </div>\n\
						</div> ');
            }
            $('#dataConfirmModal').find('.modal-body').html('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>' + $(this).attr('data-confirm'));
            $('#dataConfirmOK').attr('href', href);
            $('#dataConfirmModal').modal({show: true});
            return false;
        });
    }
    confirmation();
});






