<?php

return[
    'Users' => [
        'create-users' => 'Add users',
        'edit-users' => 'Edit users',
        'view-users' => 'View users',
        'delete-users' => 'Delete users',
    ],
    'Countries' => [
        'create-countries' => 'Add countries',
        'edit-countries' => 'Edit countries',
        'view-countries' => 'View countries',
        'delete-countries' => 'Delete countries',
    ],
    'Pages' => [
        'create-pages' => 'Add pages',
        'edit-pages' => 'Edit pages',
        'view-pages' => 'View pages',
        'delete-pages' => 'Delete pages',
    ],
    'Posts' => [
        'create-posts' => 'Add posts',
        'edit-posts' => 'Edit posts',
        'view-posts' => 'View posts',
        'delete-posts' => 'Delete posts',
    ],
    'Products' => [
        'create-products' => 'Add products',
        'edit-products' => 'Edit products',
        'view-products' => 'View products',
        'delete-products' => 'Delete products',
    ],
    'Reviews' => [
        'view-reviews' => 'View reviews',
        'delete-reviews' => 'Delete reviews',
    ],
    'Slides' => [
        'create-slides' => 'Add slides',
        'edit-slides' => 'Edit slides',
        'view-slides' => 'View slides',
        'delete-slides' => 'Delete slides',
    ],
    'Configs' => [
        'edit-configs' => 'Edit configs',
    ],
    'Contacts' => [
        'view-contacts' => 'View contacts',
        'delete-contacts' => 'Delete contacts',
    ],
    'Messages' => [
        'view-messages' => 'View messages',
        'delete-messages' => 'Delete messages',
    ],
    'Vouchers' => [
        'view-vouchers' => 'View vouchers',
        'delete-vouchers' => 'Delete vouchers',
    ],
];
