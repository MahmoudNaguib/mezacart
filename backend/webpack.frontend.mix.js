const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'https://fonts.googleapis.com/css?family=Lato:300,400,700,900',
    'resources/front/css/bootstrap.min.css',
    'resources/front/css/animate.min.css',
    'resources/front/css/owl.carousel.min.css',
    'resources/front/css/owl.theme.default.min.css',
    'resources/front/css/font-awesome.min.css',
    'resources/front/css/swiper.min.css',
    'resources/front/css/color1.css',
    'resources/front/css/custom.css'
], 'public/front/css/app.css');

mix.scripts([
    'resources/front/js/jquery-3.3.1.min.js',
    'resources/front/js/bootstrap.min.js',
    'resources/front/js/popper.min.js',
    'resources/front/js/owl.carousel.min.js',
    'resources/front/js/tilt.jquery.js',
    'resources/front/js/script.js',
    'resources/front/js/swiper.min.js',
    'resources/front/js/scroll.js',
    'resources/front/js/slider.js',
    'resources/front/js/notify.min.js',
    'resources/front/js/custom.js',
], 'public/front/js/app.js');

if (mix.inProduction()) {
    mix.version();
}



//mix.browserSync('http://localhost:8000');


